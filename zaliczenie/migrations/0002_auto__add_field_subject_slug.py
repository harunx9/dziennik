# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Subject.slug'
        db.add_column(u'zaliczenie_subject', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='asdf', unique=True, max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Subject.slug'
        db.delete_column(u'zaliczenie_subject', 'slug')


    models = {
        u'zaliczenie.event': {
            'Meta': {'object_name': 'Event'},
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['zaliczenie.Student']", 'symmetrical': 'False'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Subject']"})
        },
        u'zaliczenie.grade': {
            'Meta': {'object_name': 'Grade'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.GradeCategory']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Student']"}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.Subject']"}),
            'value': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        u'zaliczenie.gradecategory': {
            'Meta': {'object_name': 'GradeCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_value': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'zaliczenie.student': {
            'Meta': {'object_name': 'Student'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['zaliczenie.StudentGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identity': ('django.db.models.fields.CharField', [], {'max_length': '20', 'db_index': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '80', 'db_index': 'True'})
        },
        u'zaliczenie.studentgroup': {
            'Meta': {'object_name': 'StudentGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'zaliczenie.subject': {
            'Meta': {'object_name': 'Subject'},
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['zaliczenie.StudentGroup']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['zaliczenie']