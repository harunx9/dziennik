#coding: utf-8
# plik: zaliczenie/urls.py

from django.conf.urls import patterns, include, url
from django.views.generic import ListView, DetailView
from .models import Subject

urlpatterns = patterns('',
    url(r'^$', ListView.as_view(model=Subject)),  # lista przedmiotów
    url(r'^przedmiot/$',ListView.as_view(model=Subject)),  # lista przedmiotów
    url(r'^przedmiot/(?P<slug>[\w-]+)/$', DetailView.as_view(model=Subject)),  # konkretny przedmiot
)
