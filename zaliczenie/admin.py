#coding: utf-8
from django.contrib import admin

from .models import Student, StudentGroup, Event, Subject, GradeCategory, Grade
# Register your models here.

admin.site.register([StudentGroup, Event, Subject, GradeCategory, Grade])

class MyAdmin(admin.ModelAdmin):
    actions_on_bottom = True
    save_on_top = True


class GradeInline(admin.TabularInline):
    model = Grade
    extra = 1
 

class SubjectAdmin(MyAdmin):
    pass


class StudentAdmin(MyAdmin):
    list_display = ['last_name', 'first_name', 'identity', 'email', 'group']
    list_filter = ['group']
    list_editable = ['identity', 'email', 'group']
    search_fields = ['last_name', 'group__name']
    ordering = ['last_name', 'first_name']
    inlines = [GradeInline, ]
    
    
admin.site.register(Student, StudentAdmin)
